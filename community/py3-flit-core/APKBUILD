# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=py3-flit-core
pkgver=3.5.1
pkgrel=0
pkgdesc="simple packaging tool for simple packages (core)"
url="https://flit.readthedocs.io/"
arch="noarch"
license="BSD-3-Clause"
makedepends="py3-installer"
checkdepends="py3-pytest py3-testpath"
source="https://files.pythonhosted.org/packages/source/f/flit/flit-$pkgver.tar.gz"
builddir="$srcdir/flit-$pkgver/flit_core"
options="!check" # py3-testpath depends on this

# split from py3-flit, allow replacing files
replaces="py3-flit<3.5.1-r3"

build() {
	python3 build_dists.py
}

check() {
	python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/flit_core-$pkgver-py3-none-any.whl

	# remove installed tests
	rm -r "$pkgdir"/usr/lib/python3*/site-packages/flit_core/tests
}

sha512sums="
00e884774c7f59dfb54d6db09f65ac0ed47f1dd23872d6913f3a41de7242fb3829b2edacd03d08e080635c0a515521333aa74e6d26a2faa5fc02e56454b2b37a  flit-3.5.1.tar.gz
"
