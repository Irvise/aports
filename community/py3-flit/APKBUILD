# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=py3-flit
pkgver=3.5.1
pkgrel=3
pkgdesc="simple packaging tool for simple packages"
url="https://flit.readthedocs.io/"
arch="noarch"
license="BSD-3-Clause"
depends="
	py3-docutils
	py3-flit-core
	py3-requests
	py3-tomli
	py3-tomli-w
	"
makedepends="
	py3-installer
	"
checkdepends="
	py3-pytest-cov
	py3-responses
	py3-testpath
	"
source="https://files.pythonhosted.org/packages/source/f/flit/flit-$pkgver.tar.gz
	tests-use-python3.patch
	"
builddir="$srcdir/flit-$pkgver"

build() {
	# temp hack to not pull in all of py3-build
	mkdir dist
	python3 - <<-EOF
		import flit_core.buildapi as buildapi
		name = buildapi.build_wheel('./dist')
		print(name)
	EOF
}

check() {
	# Necessary for sdist tests
	git init -q
	git add .
	git config user.email "test@test.test"
	git config user.name "test"
	git commit -qm.

	# pep621 - needs pip
	# invalid_classifier - needs network access
	pytest -v -k "not test_install_module_pep621 and not test_symlink_module_pep621 and not test_invalid_classifier"
	rm -rf .git
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/flit-$pkgver-py3-none-any.whl
}

sha512sums="
00e884774c7f59dfb54d6db09f65ac0ed47f1dd23872d6913f3a41de7242fb3829b2edacd03d08e080635c0a515521333aa74e6d26a2faa5fc02e56454b2b37a  flit-3.5.1.tar.gz
0230150d17cfa1590fc8cc7fee06896d4dcc52dada50415d1035332f5a2d5d6f2970bf5e6adf34750761649c8285fa1969cc0261c60f470e89231bac466c88fb  tests-use-python3.patch
"
