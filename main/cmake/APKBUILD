# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=cmake
pkgver=3.23.0
pkgrel=0
pkgdesc="Cross-platform, open-source make system"
url="https://www.cmake.org/"
arch="all"
license="BSD-3-Clause"
makedepends="bzip2-dev curl-dev expat-dev libarchive-dev linux-headers
	libuv-dev ncurses-dev rhash-dev xz-dev zlib-dev py3-sphinx"
options="!check"
checkdepends="file musl-utils"
subpackages="$pkgname-doc $pkgname-bash-completion
	"
case $pkgver in
*.*.*.*) _v=v${pkgver%.*.*};;
*.*.*) _v=v${pkgver%.*};;
esac

source="https://www.cmake.org/files/$_v/cmake-$pkgver.tar.gz
	"

_parallel_opt() {
	local i n
	for i in $MAKEOPTS; do
		case "$i" in
			-j*) n=${i#-j};;
		esac;
	done
	[ -n "$n" ] && echo "--parallel $n"
}

build() {
	# jsoncpp needs cmake to build so to avoid recursive build
	# dependency, we use the bundled version of jsoncpp.
	# Do NOT remove --no-system-jsoncpp unless you consulted
	# maintainer
	./bootstrap \
		--prefix=/usr \
		--mandir=/share/man \
		--datadir=/share/$pkgname \
		--docdir=/share/doc/$pkgname \
		--sphinx-man \
		--system-libs \
		--no-system-jsoncpp \
		$(_parallel_opt)
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE bin/ctest
}

package() {
	cd $startdir/src/$pkgname-$pkgver
	make DESTDIR="$pkgdir" install
}
sha512sums="
bcde8f2bf2fff6c4ab37a28c115b4b53d5fef0d4e38305420966cbd9f0026a4ffdcd4137f917a83458c1f380a137f7a7bd78f6fbd4d92fdcc5cf1dfbe4c02003  cmake-3.23.0.tar.gz
"
